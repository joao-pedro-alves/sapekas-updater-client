# -*- coding: utf-8 -*-
import time

def log(msg):
	"""Função para registrar o horário de cada ação
	"""
	log = open('log.txt', 'a')
	t = time.localtime()
	string = "[%02d:%02d:%02d] %s" % (t.tm_hour, t.tm_min, t.tm_sec, msg)
	print string
	log.write(string + "\n")
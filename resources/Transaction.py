# -*- coding: utf-8 -*-

import sys
from resources.DB import *

class Transaction():
	@staticmethod
	def get_transaction(tid):
		"""Obtem informações de uma transação específica"""
		query = "SELECT TRNSEQ, TRNVLR FROM TRANSACAO WHERE TRNSEQ = " + tid
		try:
			return DB.query(query)[0]
		except IndexError:
			return False

	@staticmethod
	def get_hour_last_transaction(date):
		"""Retorna o horário da última transação realizada
		em um dia específico"""
		query = "SELECT FIRST 1 TRNHOR FROM TRANSACAO WHERE TRNDAT='"+ date +"' AND TRNVLR > 0 ORDER BY TRNHOR DESC"
		try:
			return DB.query(query)[0][0]
		except IndexError:
			return 0

	@staticmethod
	def get_by_hour(date, hour):
		"""Retorna as transações de um dia específico
		a partir de uma hora determinada"""
		query = """
			SELECT
				FINALIZACAO.TRNSEQ,
				LOJCOD,
				TRNVLR,
				FINALIZACAO.TRNDAT,
				TRNHOR,
				FUNDES,
				TRANSACAO.TRNTIP,
				FZDSEQ,
				FINALIZACAO.FZDCOD
			FROM
				FINALIZACAO
			LEFT JOIN
				TRANSACAO
			ON
				(FINALIZACAO.TRNSEQ = TRANSACAO.TRNSEQ)
			LEFT JOIN
				FUNCIONARIO
			ON
				FUNCIONARIO.FUNCOD = TRANSACAO.FUNCOD
			WHERE
				FINALIZACAO.TRNDAT='"""+ date +"""'
				AND
				TRANSACAO.TRNHOR > '"""+ str(hour) +"""'
		"""
		return DB.query(query)

	@staticmethod
	def get_items(tid):
		"""Retorna os items vendidos em uma transação"""
		query = """
			SELECT
				TRNSEQ,
				PROCOD,
				ITVQTDVDA,
				ITVVLRUNI,
				ITVTIP
			FROM
				ITEVDA
			WHERE
				TRNSEQ = """+ tid +"""
		"""
		return DB.query(query)
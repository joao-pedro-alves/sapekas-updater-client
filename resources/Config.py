# -*- coding: utf-8 -*-

import json

class Config:
	content = ''

	def __init__(self):
		with open('config.json', 'r') as content:
			self.content = json.load(content)

	def get(self):
		return self.content
# -*- coding: utf-8 -*-

import kinterbasdb
import json
import decimal
import datetime
import sys

from resources.Config import *

class DB(object):
	host 		= ''
	database 	= ''
	user 		= ''
	password 	= ''
	conn 		= False
	dbId 		= -1

	@staticmethod
	def connect(dbId = 0):
		"""Inicializa conexão com o banco de dados"""
		if DB.dbId != dbId:
			DB.load_configs(dbId)
			DB.conn = kinterbasdb.connect(host=DB.host, database=DB.database, user=DB.user, password=DB.password)
			DB.dbId = dbId

	@staticmethod
	def load_configs(dbId):
		"""Carrega as configurações de acesso ao BD"""
		config = Config().get()
		DB.host 		= str(config['db']['host'])
		DB.database 	= str(config['db']['database'][dbId])
		DB.user 		= str(config['db']['user'])
		DB.password 	= str(config['db']['password'])

	@staticmethod
	def query(query, dbId = 0):
		"""Executa uma query e retorna a lista dos resultados"""
		DB.connect(dbId)
		cur = DB.conn.cursor()
		cur.execute(query)

		if query.find('SELECT') == -1:
			DB.conn.commit()

		# Caso a query seja um SELECT, o resultado será
		# em forma de uma lista de dados. Caso seja
		# QUALQUER OUTRA OPERAÇÃO, entao o resultado
		# será um boleano verdadeiro
		try:
			return [x for x in cur]
		except kinterbasdb.ProgrammingError:
			return True

	@staticmethod
	def json_encode(data):
		"""Converte uma lista de dados em uma string JSON"""
		return json.dumps([x for x in data], default=DB.parse_json_response)

	@staticmethod
	def parse_json_response(data):
		"""Usada para o tratamento dos dados da uma lista
		para o formato JSON. Esse método transforma alguns
		tipos especiais em um formato hábil para ser convertido
		em uma string e JSON. Esse método nunca deve ser chamado
		explicitamente"""
		if isinstance(data, decimal.Decimal):
			return float(data)	
		elif isinstance(data, datetime.datetime):
			return data.strftime('%Y/%m/%d')
		return data
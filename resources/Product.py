# -*- coding: utf-8 -*-

import sys
from datetime import date

from resources.DB import *
from resources.Utils import *

class Product(object):
	@staticmethod
	def add(data):
		"""
		"""
		today = date.today().strftime('%m/%d/%Y')

		for d in data:
			query = """
			INSERT INTO
				PRODUTO
			(
				PROCOD,
				PROCODINT,
				PRODES,
				PRODESRDZ,
				SECCOD,
				TRBID,
				PROUNID,
				PRODCNMAX,
				PROPRCVDAVAR,
				PROPRCOFEVAR,
				PROVLD,
				LOCCOD,
				PROFRTLOJ,
				PROPRCDATALT,
				PROFLGALT,
				PROTABA,
				PROPRC1,
				PROBONTIP,
				PROCTREST,
				PROPERDCN,
				PROCOMP,
				PROMRG1,
				PROMRG2,
				PROMRG3,
				PRODATCADINC,
				FUNCOD,
				PRODESVAR,
				PROQTDMINPRC2,
				PROQTDMINPRC3,
				GRPCOD,
				PROITEEMB,
				PROCSTMED,
				PROQTDMAXVDA,
				PROPESLIQ,
				PROFORLIN,
				PROUNDCMP,
				PROIAT,
				PROIPPT,
				PROUNDREF,
				PROMEDREF,
				PROPRCCSTMED,
				PROPRCCSTFIS,
				PRONCM,
				PROSTAPAF
			) VALUES (
				'"""+ d['PROCOD'] +"""',
				NULL,
				'"""+ d['PRODES'] +"""',
				'"""+ d['PRODESRDZ'] +"""',
				'01',
				'T19',
				'UN',
				NULL,
				"""+ d['PROPRCVDAVAR'] +""",
				"""+ d['PROPRCOFEVAR'] +""",
				0,
				'00',
				'N',
				'"""+ today +"""',
				'A',
				0,
				"""+ d['PROPRC1'] +""",
				'N',
				'S',
				'N',
				'N',
				NULL,
				NULL,
				NULL,
				'"""+ today +"""',
				'000001',
				NULL,
				NULL,
				NULL,
				'000',
				1,
				NULL,
				NULL,
				NULL,
				'N',
				'UN',
				'A',
				'T',
				'UN',
				1,
				0,
				0,
				'61042200',
				'?'
			)
			"""

			try:
				DB.query(query)
			except:
				log("Parece que houve uma tentativa de atualizar um produto que já está atualizado")
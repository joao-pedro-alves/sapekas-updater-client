# -*- coding: utf-8 -*-

import requests
import time
import sys
from datetime import date

from resources.Transaction import *
from resources.Utils import *
from resources.Product import *

class Service:
	@staticmethod
	def start_product_update():
		"""
		"""
		config = Config().get()
		url = config['base_url'] + '/service/productupdate'

		log('# Esperando por atualizações de produtos')
		while True:
			time.sleep(60)
			response = requests.post(url, data = {
				'content' 	: '003',
				'action' 	: 'update_client',
				'key'  		: config['service']['key']
			});

			if response.text != '0':
				data = json.loads(response.text)
				log(str(len(data)) + ' novo(s) produto(s) encontrado(s). Iniciando atualização...')
				Product.add(data)
				log('Atualização dos produtos feita com sucesso! ;)')
				log('# Esperando por atualizações de produtos')

	@staticmethod
	def start_transaction_update():
		"""
		"""
		log('# Esperando por atualizações de transições')
		while True:
			time.sleep(3)
			try: 
				today = date.today().strftime('%m/%d/%Y')

				last_update_server = Service.check_updates()
				last_update_client = Transaction.get_hour_last_transaction(today)

				if last_update_server == 'ACCESS_DANIED':
					log('Senha de segurança do servidor está incorreta!')
					sys.exit()

				# Se a hora do servidor for MENOR que a hora do cliente
				# então deve ser feita a atualização
				if last_update_server < last_update_client:
					log('Atualizações encontradas. Iniciando atualizações')
					Service.transaction_update(today, last_update_server)
			except requests.exceptions.ConnectionError:
				log('- Falha ao se conectar a internet')

	@staticmethod
	def transaction_update(date, hour):
		"""Rotina de atualização das transferências"""
		config = Config().get()

		log('Obtendo transações...')
		transactions = Transaction.get_by_hour(date, hour)
		items = []

		log('Obtendo produtos...')
		for t in transactions:
			for i in Transaction.get_items(t[0]):
				items.append(i)

		log('Preparando transações e produtos para o envio...')
		items = DB.json_encode(items)
		transactions = DB.json_encode(transactions)

		config = Config().get()
		url = config['base_url'] + '/service/transactionupdate'

		log('Enviando transações para o servidor...')
		requests.post(url, data = {
			'content' 	: transactions,
			'action' 	: 'update_transactions',
			'key'  		: config['service']['key']
		});

		log('Transações enviadas para o servidor com sucesso!')
		log('Enviando produtos para o servidor...')

		requests.post(url, data = {
			'content' : items,
			'action'  : 'update_transaction_items',
			'key'	  : config['service']['key']
		});

		log('Produtos enviados para o servidor com sucesso!')
		log('Atualização no servidor realizada com sucesso! ;)')
		log('# Esperando por novas atualizações...')


	@staticmethod
	def check_updates():
		"""
		Manda uma requisição para o servidor junto com
		a data do dia atual e recebe do servidor a hora
		da última transição armazenada no banco de dados
		do dia enviado. Caso o servidor não possua nenhuma
		transição desse dia armazenada então será retornado 0
		"""
		config = Config().get()
		url = config['base_url'] + '/service/transactionupdate'
		t = date.today()
		data = {
			'content' : '{0}/{1}/{2}'.format(t.month, t.day, t.year),
			'action' : 'verify_last_update',
			'key' : config['service']['key']
		}
		response = requests.post(url, data = data);
		return response.text

# -*- coding: utf-8 -*-

import threading
import sys

from resources.Transaction import *
from resources.Service import *
from resources.Utils import *
from resources.Product import *

if __name__ == '__main__':
	transaction_update = threading.Thread(target=Service.start_transaction_update)
	transaction_update.start()

	#product_update = threading.Thread(target=Service.start_product_update)
	#product_update.start()
	